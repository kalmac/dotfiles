# Dotfiles

Contains my aliases :

* apache - summon apache to serve current directory on port 80
* composer
* npm
* mongoimport
* php

+ other shortcuts... see [.aliases](/.aliases)

All you need is [docker](https://www.docker.com/community/open-source) !
And if you don't have it, it will ask you to install it with [get.docker.com](https://get.docker.com)
See [.aliases](/.aliases) for details.

# Installation

1. Check out a clone of this repo to a location of your choice : git clone git@gitlab.com:kalmac/dotfiles.git .
2. Run install.sh [-f]
3. Restart your terminal

# Why docker ?
Many dev commands available on a minimal linux installation 
with only one system modification. Easy !
