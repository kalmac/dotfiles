#!/bin/bash

usage() { echo "Usage : $0 [-f]" 1>&2; exit 1; }
force=0

link () {
  if [ $force = 1 ]; then
    rm ~/$1 -rf
  elif [ -e "$(pwd)/$1" ]; then
    echo "$1 exists... aborting" >&2
    return
  fi

  ln -s $(pwd)/$1 ~/$1
}


while getopts ":f" opt; do
  case $opt in
    f)
      force=1
      echo "Force was used ... existing files will be deleted" >&2
      ;;
  esac
done

link .aliases
link .bash_aliases

