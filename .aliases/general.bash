# Aliases in alphabetical order
alias c='clear'
alias cp='cp -iv'
alias df='df -h'
alias dir='dir -F --color=always'
alias grep='grep --color=auto -in'
alias l='ls -lah'
alias md='mkdir -p'
alias mv='mv -iv'
alias rm='rm -i'
alias x='exit'

# Navigation enhancement.
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'
alias .......='cd ../../../../../..'

