# Aliases in alphabetical order
alias composer="docker_run /app composer"
alias mongoimport="docker_run /app mongo mongoimport"
alias npm="docker_run /app node:current-alpine npm"
alias php="docker_run /app php"
alias node="docker_run /app node:current-alpine node"

# apache - summon apache to serve current directory on port 80
function apache() {
  docker_install
  docker run -d --rm \
    -p 80:80 \
    -v $(pwd):/usr/local/apache2/htdocs/ \
    httpd:alpine
}

# openrefine - Launch openrefine on port 3333
function openrefine() {
  docker_install
  docker volume create openrefine_data
  docker run -d --rm \
    -p 3333:3333 \
    -v openrefine_data:/data \
    vimagick/openrefine
}

# portainer - Launch portainer on port 9000
function portainer() {
  docker_install
  docker volume create portainer_data
  docker run -d --rm \
    -p 9000:9000 \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v portainer_data:/data \
    portainer/portainer
}

# docker_run [in directory] [docker image with commands]
function docker_run() {
  docker_install
  docker run -it --rm --net="host" \
    -u $(id -u):$(id -g) \
    -v $(pwd):$1 -w $1 \
    ${@:2}
}

# docker_install - with https://get.docker.com
function docker_install() {
  if ! hash docker 2>/dev/null; then

    read -p "Install docker with get.docker.com (y/n)? " -n 1 -r
    echo    # (optional) move to a new line
    if [[ ! $REPLY =~ ^[Yy]$ ]]
    then
        [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell
    fi

    curl -fsSL https://get.docker.com -o /tmp/get-docker.sh
    sh /tmp/get-docker.sh

  fi
}

